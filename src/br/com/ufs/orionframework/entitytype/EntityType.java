package br.com.ufs.orionframework.entitytype;

import br.com.ufs.orionframework.entity.Attrs;

/**
 * This class is used to help to represent a entity Type as java object representing the JSON on a NGSIv2 form.
 *
 * @author Romario Bispo
 * @version %I%, %G%
 * @since 1.0
 * @see br.com.ufs.orionframework.orion.Orion
 */
public class EntityType {
    private String type;
    private Attrs attrs;
    private int count;
}
