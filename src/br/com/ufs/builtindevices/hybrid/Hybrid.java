package br.com.ufs.builtindevices.hybrid;

import br.com.ufs.orionframework.entity.Entity;

/**
 * This class represents a hybrid device, all hybrid entity must inherit this class.
 */
public class Hybrid extends Entity {

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setType(String type) {
        this.type = type;

    }
    @Override
    public void setId(String id) {
        this.id = id;
    }
}
