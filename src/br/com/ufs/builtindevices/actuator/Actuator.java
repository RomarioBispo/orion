package br.com.ufs.builtindevices.actuator;

import br.com.ufs.orionframework.entity.Entity;

/**
 * This class represents a actuator, all actuator entity must inherit this class.
 */
public abstract class Actuator extends Entity {
    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void setId(String id) {
        this.id = id;

    }
}
