package br.com.ufs.builtindevices.sensor;

import br.com.ufs.orionframework.entity.Entity;

/**
 * This class represents a sensor device, all sensor entity must inherit this class.
 */
public class Sensor extends Entity {

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setType(String type) {
        this.type = type;

    }

    @Override
    public void setId(String id) {
        this.id = id;
    }
}
